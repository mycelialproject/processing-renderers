import netP5.*;
import oscP5.*;
import spout.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;



// GLOBAL IMPORTANT PARAMS
// Pixels per update.
float   scroll_speed = 0.5;
int     update_hertz = 2;
boolean SPOUT_ENABLED = false;
String  FONTFACE = "Arial Bold";
int SHOWSTARTID = 5334 ;


// String[] LQS = {"Q1","Q2","Q3","Q4","Q5","Q6","Q7","Q8" };
String[] LQS = {"WAIT1", "WAIT2", "WAIT3", "WAIT4", "WAIT5"};
String DATAURL = "http://178.128.66.26/index.php";

// For multiplying
final int SECONDS = 1000;

// SPOUT //
Spout spout;

PGraphics pgr;

// Controlling how many rows of text can be animated at once.
final int ROWS_OF_TEXT = 8;
TextLine[] tlines = new TextLine[ROWS_OF_TEXT];


// MCJ NOTE: This is a small state machine. So, I'll 
// use some 'if' statements to keep things clear. The 
// switch is nice, but Processing doesn't know how to indent it.
// Also, it's not reading it as a block... so, we have duplicate 
// variable declarations.

// MESSAGE DATABASE
// The use of a hashmap guarantees uniqueness on the key,
// even if we do multiple inserts.
HashMap<String, Message> db = new HashMap<String, Message>();

// Update in the background.

/* fetchMessages */
int last_fetch = millis();
void fetchMessagesThread () {
  while (true) {
    int now = millis();

    if ((now - last_fetch) > (1000 / update_hertz)) {
      last_fetch = now;
      fetchMessages();
    }

    delay(10 * SECONDS);
  }
}

void fetchMessages() {
  JSONObject jsonMessages = new JSONObject();

  try {
    // println("[GRAB] Grabbing messages from server.");
    jsonMessages = loadJSONObject(DATAURL);
  } 
  catch (Exception e) {
    println("[GRAB] Failed.");
    println();
    println(e);
  }

  int message_count = 0;
  // Load into the local DB. The hashmap guarantees uniqueness.
  if (jsonMessages != null) {
    if (jsonMessages.getInt("status") == 1) {
      JSONArray arr = jsonMessages.getJSONArray("data");
      for (int row_ndx = 0; row_ndx < arr.size(); row_ndx++) {
        // println(row_ndx);
        JSONObject item = arr.getJSONObject(row_ndx);

        for (int usr_ndx = 0; usr_ndx < arr.size(); usr_ndx++) {
          //load row into a var
          JSONObject msg = arr.getJSONObject(usr_ndx);
          if (item != null) {
            Message m = new Message(msg);
            // Make sure it is approved.
            if (m.approved) {
              db.put(m.id, m);
              message_count++;
            }

            // println("[DB ADD] " + m);
            // println("\t[M TAG] " + m.tag);
            // println("\t[M MSG] " + m.msg);
          }
          // delay(3000);

          // println("[MC] retrieved " + message_count + " db " + db.size());
        }
      }
    }
  }
} 

/* filterOnTag 
 * Grabs the most recent messages, and filters them on the tag passed in.
 * Returns it as... 
 *
 */
HashMap<String, Message> filterOnTag (String tag) {
  HashMap<String, Message> filtered = new HashMap<String, Message>();

  // https://stackoverflow.com/questions/4234985/how-to-for-each-the-hashmap
  for (Map.Entry<String, Message> msg : db.entrySet()) {
    String key = msg.getKey();
    Message value = msg.getValue();

    // println("[TAG] " + value.tag + " - " + tag);

    // CAUTION: Don't compare Java strings with ==
    // That compares the pointers.
    if (value.tag.equals(tag)) {
      // println("HUZZAH THEY ARE THE SAME! EXULTATE EQUALUS!");
      filtered.put(value.id, value);
    }
  }

  //println("[filterOnTag] [" + tag + "] size: " + filtered.size());
  return filtered;
}

HashMap<String, Message> selectRandom(int numberOfMessages, HashMap<String, Message> messages) {
  HashMap<String, Message> random = new HashMap<String, Message>();
  // FIXME: The messages hashmap could have zero, or fewer than the number of messages we desire.
  // We shouldn't run into this... there's already 5000+ messages in the DB.
  // So, we'll always have the last 150. But, it's a source of possible error in the future.
  println("messages.size(): " + messages.size());
  if (messages.size() > 0) {
    for (int i = 0 ; i < numberOfMessages; i++) {
      // println("[selectRandom] random.size() : " + random.size());
      Random generator = new Random();
      Object[] values = messages.values().toArray();
      Message randomMessage = (Message)values[generator.nextInt(values.length)];
      int intID = Integer.parseInt(randomMessage.id);
      if (intID > SHOWSTARTID) {
        random.put(randomMessage.id, randomMessage);
      }
      // println("I put: " + randomMessage);
      // println("id: " + randomMessage.id);
    }
  }
  return random;
}

HashMap<String, Message> selectRandomFromTag(int numberOfMessages, String tag) {
  HashMap<String, Message> random = selectRandom(numberOfMessages, filterOnTag(tag));

  return random;
}



class TextLine {

  private int     FONTSIZE    = 50;
  private Message message     = null;
  private int     fontSize    = FONTSIZE;
  private PFont   pf          = createFont(FONTFACE, fontSize, true);
  private int     index       = 0; 
  private int     birth       = 0;
  private int     lifetime    = (12 + (int)floor(random(8))) * SECONDS;

  private float posX, posY;

  TextLine(Message message, int ndx) {
    this.index = ndx;
    // println("INDEX: " + this.index);
    init(message);
  }

  void init (Message message) {
    this.message = message;
    // posX = width + random(400);
    if (textWidth(this.message.msg) > width) {
      posX = 80;
    } else {
      posX = 0;
    }

    posY = ((height / 8) * (this.index + 1)) - (FONTSIZE / 4);
    // println("posY " + posY);

    birth = millis();
  }

  int c = 1;
  public void draw () {

    textFont(pf);
    fill(255);
    textAlign(LEFT);

    if (textWidth(this.message.msg) > width) {
      posX -= scroll_speed;
    }

    text(message.msg, posX, posY);

    // Pull ourselves out of the global drawing queue.
    if (((millis() - birth) > lifetime) && (posX + textWidth(this.message.msg) + 50 < width)) {
      tlines[index] = null;
    }
  }
}

void setup() {
  size(1280, 720, P3D);
  pgr = createGraphics(width, height);
  // oscP5 = new OscP5(this, OSC_PORT);

  for (int i = 0; i < tlines.length; i++) {
    tlines[i] = null;
  }

  // Initial DB update.
  fetchMessages();
  // Spawn the thread.
  thread("fetchMessagesThread");
  
  spout = new Spout(this);
  spout.createSender("mosho-text-waiting-tag");
}

void draw() {
background(0,0,0,0);
  // Fetch the most recent messages.

  for (int ndx = 0; ndx < tlines.length; ndx++) {
    // If we find a blank slot, create a new TextLine.
    if (tlines[ndx] == null) {

      HashMap<String, Message> accumulator = new HashMap<String, Message>();
      for (String LQ : LQS) {
        // println("TAG: " + LQ);
        HashMap<String, Message> results = selectRandomFromTag(10, LQ);
        // println("RESULTS: " + results.size());
        accumulator.putAll(results);
      }
      // println("ACCUM: " + accumulator.size());
      HashMap<String, Message> results = selectRandom(1, accumulator);
      for (Map.Entry<String, Message> msg : results.entrySet()) {
        String k = msg.getKey();
        Message m = msg.getValue();
        TextLine t = new TextLine(m, ndx);
        tlines[ndx] = t;
      }
    } else {
      // We found a live one. Draw it.
      // Well, update it.
      tlines[ndx].draw();
    }
  }
  
  spout.sendTexture();
  
}
