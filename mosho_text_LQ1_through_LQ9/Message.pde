class Message
{
  String id;
  String tag;
  String msg;
  boolean approved;

  Message(JSONObject item)
  {
    //pp(item.getString("messageID"));
    this.id        = item.getString("messageID");
    this.tag       = item.getString("tag");
    this.msg       = item.getString("message");
    if (item.getString("approved").equals("1")) {
      this.approved = true;
    } else {
      this.approved = false;
    }
    this.msg       = msg.replace("|", " - ");
  }

  Message() {
    this.id        = createGUIDString();
    this.tag       = "pad msg";
    this.msg       = "";
  }

  //generate a random id
  String createGUIDString()
  {

    String[] a = {"1", "2","3","4","5","6","7","8","9","0","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","r","t","u","v","w","x","y","z"};
    String TempString = a[int(random(a.length))];

    for (int i = 0; i < 36 - 1; i++)
    {
      TempString = TempString + a[int(random(a.length))];
    }
    return TempString;
  }
 }
